import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BusFareTest {

	
	double expectedOutput;
	
	@Before
	public void setUp() throws Exception {
		
		
	}

	
	// Naming Routes
	
	// leslie to donmills or vice versa = route1
	// finch to sheppard or vice versa = route2
	// sheppard to leslie or vv = route3
	
	@Test
	public void zone1oneWayTest() {
		BusFare fare = new BusFare();
		 String[] route = { "route1"};
		 expectedOutput = 2.50;
		assertEquals(expectedOutput,fare.fareCount(route),0);
		
	}
	
	
	@Test
	public void zone2oneWayTest() {
		BusFare fare = new BusFare();
		String[] route ={ "route2"};
		 expectedOutput = 3.00;
		assertEquals(expectedOutput,fare.fareCount(route),0);
		
	}
	
	@Test
	public void between2ZonesTest() {
		BusFare fare = new BusFare();
		String[] route ={ "route1","route2", "route3"};
		 expectedOutput = 3.00;
		assertEquals(expectedOutput,fare.fareCount(route),0);
		
	}
	
	@Test
	public void multipleTripTest() {
		BusFare fare = new BusFare();
		String[] route ={ "route2","route1"};
		 expectedOutput = 5.50;
		assertEquals(expectedOutput,fare.fareCount(route),0);
		
	}
	
	@Test
	public void maximumTripTest() {
		BusFare fare = new BusFare();
		String[] route ={ "route2","route2","route2"};
		 expectedOutput = 6.00;
		assertEquals(expectedOutput,fare.fareCount(route),0);
		
	}

}
